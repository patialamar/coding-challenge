import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';

@Pipe({ name: 'sanitizeHtml'})
export class SafePipe implements PipeTransform {
 constructor(private sanitizer:DomSanitizer) { }
   transform(value:string):SafeHtml {
    return this.sanitizer.bypassSecurityTrustHtml(value);
  }
}

