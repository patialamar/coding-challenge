import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { UserService } from '../services/user.service';

// import { ToastComponent } from '../shared/toast/toast.component';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  @Input('heading') heading;
  public users: any;
  public name: any;
  public email: any;
  public amount: any;
  public gender: any;
  public dateOfBirth: any;
  public status: any;
  public action:any;
  public id:any;
  userForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    // public toast: ToastComponent,
    private route: ActivatedRoute,
    private router: Router,
    public userService: UserService) {
    this.userForm = formBuilder.group({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      dateOfBirth: new FormControl('', [Validators.required]),
      gender: new FormControl('', [Validators.required]),
      amount: new FormControl('', [Validators.required]),

    });
  }

  ngOnInit() {
    this.list();
    this.status = false;
  }


  /*========================================= CREATE CONTACT =================================================*/


  create_contact() {
    var obj = {
      name: this.name,
      email: this.email,
      gender: this.gender,
      amount: this.amount,
      dateOfBirth: this.dateOfBirth,
    }
    this.userService.create(obj).subscribe(data => {
      // this.toast.setMessage("language is created successfully.", 'success');
      this.userForm.reset();
      this.status = false;
      return true;
    }, error => {
      console.log(error);
    });
  }
goTo(userId){
   this.router.navigate(['/contact-detail', userId]);
}

  /*========================================= EDIT CONTACT =================================================*/

  edit(data) {
    this.action = data ? 'update': 'save';
    this.status = true;
    this.name = data.name;
    this.email = data.email;
    this.gender = data.gender;
    this.amount = data.amount;
    this.dateOfBirth = data.dateOfBirth;
    this.id = data.id;

  }

  /*========================================= UPDATE CONTACT =================================================*/

  update() {
    var obj = {
      name: this.name,
      email: this.email,
      gender: this.gender,
      amount: this.amount,
      dateOfBirth: this.dateOfBirth,
    }
    this.userService.update(obj, this.id).subscribe(data => {
      this.userForm.reset();
      this.status = false;
      this.list();
      return true;
    }, error => {
      console.log(error);
    });
  }

  /*========================================= GET  LIST=================================================*/

  list() {
    this.userService.getUserList().subscribe(
      data => {
        this.users = data.data;
        return true;
      },
      error => {
        return Observable.throw(error);
      }
    );
    return event;
  }

  validation_messages = {

    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Please enter a valid email.' }
    ]
  };
}
