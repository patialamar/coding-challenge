import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/concatMap';
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable()

export class UserService {
  public data = [];
  public token:string;
  constructor(private http:HttpClient, private router: Router
) {
   this.token = localStorage.getItem('currentUser')
}
  ngOnInit() { }
  getUserList(): Observable<any> {
     let headers = new HttpHeaders();
     headers = headers.set('Content-Type', 'application/json; charset=utf-8').set('x-access-token', ''); 
     return this.http.get(environment.apiURI + 'user' , {headers: headers});
    
  }
 
 
 
  create(data): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('x-access-token',''); 
    return this.http.post(environment.apiURI + 'user', data, {headers: headers});
  }
  
  contactDetail(id: any): Observable<any> {
     let headers = new HttpHeaders();
    headers = headers.set('x-access-token',''); 
     return this.http.get(environment.apiURI + 'user/'+ id,  {headers: headers});
  }
  update(data, id): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('x-access-token', ''); 
     return this.http.put(environment.apiURI + 'user/' + id, data, {headers: headers});
  }
  edit(id): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('x-access-token', ''); 
     return this.http.get(environment.apiURI + 'user/' + id, {headers: headers});
  }
  
}
