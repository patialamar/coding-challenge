import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router, NavigationStart, NavigationEnd} from '@angular/router';
import {Helpers} from "./helpers";
import { UserService } from './services/user.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app';
  globalBodyClass = 'm-page--loading-non-block m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default';
  
  public AppConfig: any;
  constructor(
      public router: Router,
      public userService: UserService
  ) { }


  ngOnInit() {
      this.checkIfUserIsLoggedIn()
      this.router.events.subscribe((route) => {
        if (route instanceof NavigationStart) {
          Helpers.setLoading(true);
          Helpers.bodyClass(this.globalBodyClass);
        }
        if (route instanceof NavigationEnd) {
          Helpers.setLoading(false);
        }
      });
  }


  checkIfUserIsLoggedIn(){
    // this.router.navigateByUrl('/login');
      // // check if user is logged In
      // if (
      //     !this.userService['user']
      // )
      // {
      //     this.router.navigateByUrl('/login');
      // }
  }
  
}
