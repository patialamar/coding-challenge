import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ScriptLoaderService } from "./services/script-loader.service";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule, provideRoutes,Routes} from '@angular/router';
import { UserService } from './services/user.service';
import { UserComponent } from './user/user.component';
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FileUploadModule } from 'ng2-file-upload';
import { Ng2CloudinaryModule } from 'ng2-cloudinary';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { TagInputModule } from 'ngx-chips';
import { LoadingModule } from 'ngx-loading';
import { NgxEditorModule } from 'ngx-editor';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { SafePipe } from './safe.pipe';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { EditUserComponent } from './edit-user/edit-user.component';

@NgModule({
  declarations: [
    AppComponent,
   
   
    UserComponent,
   
    SafePipe,
   
    EditUserComponent
    // ConfirmationComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPaginationModule,
    InfiniteScrollModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    Ng2CloudinaryModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([]),
    TagInputModule,
    LoadingModule,
    NgxEditorModule,
    TooltipModule

  ],
  // entryComponents: [ConfirmationComponent],
  providers: [ScriptLoaderService, UserService, {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})

export class AppModule { }