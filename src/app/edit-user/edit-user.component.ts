import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  public user:any;
  public obj:any;
  constructor(
    private route: ActivatedRoute,
    public userService: UserService) { }

  ngOnInit() {
    this.obj = this.route.snapshot.params;
    if(this.obj.id){
      this.get();
    }

  }
  get() {
    this.userService.contactDetail(this.obj.id).subscribe(data => {
      this.user = data;
      return true;
    }, error => {
      console.log(error);
    });
  }

}
