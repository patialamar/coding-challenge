import { RouterModule, Routes } from '@angular/router';
import { EditUserComponent } from './edit-user/edit-user.component';
import { UserComponent } from './user/user.component';
const AppRoutes: Routes = [
    { path: '', component: UserComponent,  pathMatch: 'full' },
   { path: 'contact-detail/:id', component: EditUserComponent},
    // { path: '**', redirectTo: 'dashboard', pathMatch: 'full' },
];

export const AppRoutingModule = RouterModule.forRoot(AppRoutes, { useHash: true });
